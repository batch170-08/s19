const cube = (num) => {
	console.log(`The cube of ${num} is ${num ** 3}`)
};

cube(2);

let address = [`258 Washington Ave`, `NW`, `California`, `90011`];
const [street, city, state, zipcode] = address;
console.log(`I live at ${street} ${city}, ${state} ${zipcode}`);

let animal = {
	name: `Lolong`,
	type: `saltwater crocodile`,
	weight: `1075 kgs`,
	size: `20 ft 3 in`
};

const {name, type, weight, size} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${size}`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach(x => console.log(x));

const sum = numbers.reduce((x, y) => x + y)
console.log(sum);

class Dog {
	constructor (name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};

const dog1 = new Dog(`Coco Biggie`, 7, `Shibaboy`);
console.log(dog1);